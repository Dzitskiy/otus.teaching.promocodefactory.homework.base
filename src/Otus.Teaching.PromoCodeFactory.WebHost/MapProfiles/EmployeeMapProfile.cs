﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.MapProfiles
{    
    /// <summary>
    /// Профиль автомаппера.
    /// </summary>
    public class EmployeeMapProfile : Profile
    {
        public EmployeeMapProfile()
        {      
            CreateMap<EmployeeShortRequest, Employee>();
            CreateMap<Employee, EmployeeShortResponse>();
            CreateMap<Employee, EmployeeResponse>();
        }
    };
}