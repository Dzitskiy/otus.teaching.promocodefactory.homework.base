﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        /// <summary>
        /// Получить всех сущностей
        /// </summary>
        Task<IEnumerable<T>> GetAllAsync();
        
        /// <summary>
        /// Получить сущность по идентифмкатору
        /// </summary>
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Создание сущности
        /// </summary>
        Task<T> CreateAsync(T entity);

        /// <summary>
        /// Измение сущности
        /// </summary>
        Task<T> UpdateAsync(T updateEntity);

        /// <summary>
        /// Удаление сущности
        /// </summary>
        Task DeleteAsync(Guid id);
    }
}