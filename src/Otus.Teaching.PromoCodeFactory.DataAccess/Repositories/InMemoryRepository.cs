﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    ///<inheritdoc cref="ITestInterface"/>
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data ?? new List<T>();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T entity)
        {
            if (entity.Id == Guid.Empty)
            {
                entity.Id = Guid.NewGuid();
            }
            Data = Data.Append(entity);
            
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == entity.Id));
        }

        public Task<T> UpdateAsync(T updateEntity)
        {
            var dataArray = Data.ToArray();
            for (int i = 0; i < dataArray.Length; i++)
            {
                if (dataArray[i].Id == updateEntity.Id)
                {
                    dataArray[i] = updateEntity;
                }
            }
            Data = dataArray.ToList();

            return GetByIdAsync(updateEntity.Id);
        }

        public Task DeleteAsync(Guid id)
        {
            return Task.FromResult(Data = Data.Where(x => x.Id != id));
        }
    }
}